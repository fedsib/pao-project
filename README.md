pao-project
===========

Project for "*Programmazione ad Oggetti*" A.A. 2015/2016 at *University of Padua*

Developed in C++ / Qt using:

- Qt Creator : 3.2.1
- Qt version: 5.3.2
- GCC: 4.8.6/5.3.0
- git: 2.9/2.10
- OS: Ubuntu 16.04 Xenial Xerus / Windows 7 Professional 64 bit
- A private repository on GitHub